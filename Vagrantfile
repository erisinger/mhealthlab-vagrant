# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # Every Vagrant virtual environment requires a box to build off of.
  # See box list
  #   http://www.vagrantbox.es/
  config.vm.box = "ubuntu/bionic64"

  # The url from where the 'config.vm.box' box will be fetched if it
  # doesn't already exist on the user's system.
  # config.vm.box_url = "http://domain.com/path/to/above.box"

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "forwarded_port", guest: 9595, host: 9595 # dcrs
  config.vm.network "forwarded_port", guest: 9696, host: 9696 # sdcrs
  config.vm.network "forwarded_port", guest: 8888, host: 8989 # jupyter
  config.vm.network "forwarded_port", guest: 5601, host: 5601 # kibana
  config.vm.network "forwarded_port", guest: 80, host: 8080   # web server
  config.vm.network "forwarded_port", guest: 8090, host: 8090 # 
  config.vm.network "forwarded_port", guest: 9200, host: 9200 # elasticsearch
#  config.vm.network "forwarded_port", guest: 9300, host: 9300 # elasticsearch (transport)
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "forwarded_port", guest: 8181, host: 8181 # airflow
  

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"
  #config.vm.network :private_network, ip: "192.168.58.111"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # If true, then any SSH connections made will enable agent forwarding.
  # Default value: false
  # config.ssh.forward_agent = true

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Don't boot with headless mode
    vb.gui = false

  #   # Use VBoxManage to customize the VM. For example to change memory:
#    vb.customize ["modifyvm", :id, "--memory", "8192"]
    vb.customize ["modifyvm", :id, "--memory", "2048"]
  end
  
  config.vm.provision "shell", privileged: false, inline: <<-SHELL 
  
    # confirm that user is vagrant (not root)
    echo Running as user $USER
    
    if [ ! -d /home/vagrant/mhealthlab-deployable ]
    then
      echo mHealthLab Deployable not found...cloning.
      git clone https://gitlab.com/erisinger/mhealthlab-deployable.git
      echo Done
    else
      echo mHealthLab Deployable already exists.  Updating.
      cd /home/vagrant/mhealthlab-deployable/
      git pull origin master
      cd /home/vagrant
      echo Done
    fi
    
    # Use scenario
#    source /home/vagrant/mhealthlab-deployable/install_and_run.sh
    source /home/vagrant/mhealthlab-deployable/install_and_run_analytics_only.sh
    
    # Airflow -- optional
    source /home/vagrant/mhealthlab-deployable/bin/autolaunch_with_airflow_up.sh
#    source /home/vagrant/mhealthlab-deployable/bin/autolaunch_standalone_up.sh
    
#    echo "Running Cerebral Cortex integration test"
#    
#    python3 /home/vagrant/mhealthlab-deployable/demos/cc_integration/cc_integration_test.py
    
  SHELL
  
  config.vm.provider :virtualbox do |vb|
    # Don't boot with headless mode
    #   vb.gui = true

    # Enable creating symlinks between guest and host
    vb.customize [
      # see https://github.com/mitchellh/vagrant/issues/713#issuecomment-17296765
      # 1) Added these lines to my config :
      #
      # 2) run this command in an admin command prompt on windows :
      #    >> fsutil behavior set SymlinkEvaluation L2L:1 R2R:1 L2R:1 R2L:1
      #    see http://technet.microsoft.com/ja-jp/library/cc785435%28v=ws.10%29.aspx
      # 3) REBOOT HOST MACHINE
      # 4) 'vagrant up' from an admin command prompt
      "setextradata", :id,
      "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant-root", "1"
    ]

    # Use VBoxManage to customize the VM. For example to change memory:
    #vb.customize [
    #  'modifyvm', :id,
    #  '--natdnshostresolver1', 'on',
    #  '--memory', '512',
    #  '--cpus', '2'
    #]
  end

  #
  # View the documentation for the provider you're using for more
  # information on available options.

  # Enable provisioning with Puppet stand alone.  Puppet manifests
  # are contained in a directory path relative to this Vagrantfile.
  # You will need to create the manifests directory and a manifest in
  # the file centos65.pp in the manifests_path directory.
  #
  # An example Puppet manifest to provision the message of the day:
  #
  # # group { "puppet":
  # #   ensure => "present",
  # # }
  # #
  # # File { owner => 0, group => 0, mode => 0644 }
  # #
  # # file { '/etc/motd':
  # #   content => "Welcome to your Vagrant-built virtual machine!
  # #               Managed by Puppet.\n"
  # # }
  #
  # config.vm.provision "puppet" do |puppet|
  #   puppet.manifests_path = "manifests"
  #   puppet.manifest_file  = "site.pp"
  # end

  # Enable provisioning with chef solo, specifying a cookbooks path, roles
  # path, and data_bags path (all relative to this Vagrantfile), and adding
  # some recipes and/or roles.
  #
  # config.vm.provision "chef_solo" do |chef|
  #   chef.cookbooks_path = "../my-recipes/cookbooks"
  #   chef.roles_path = "../my-recipes/roles"
  #   chef.data_bags_path = "../my-recipes/data_bags"
  #   chef.add_recipe "mysql"
  #   chef.add_role "web"
  #
  #   # You may also specify custom JSON attributes:
  #   chef.json = { :mysql_password => "foo" }
  # end

  # Enable provisioning with chef server, specifying the chef server URL,
  # and the path to the validation key (relative to this Vagrantfile).
  #
  # The Opscode Platform uses HTTPS. Substitute your organization for
  # ORGNAME in the URL and validation key.
  #
  # If you have your own Chef Server, use the appropriate URL, which may be
  # HTTP instead of HTTPS depending on your configuration. Also change the
  # validation key to validation.pem.
  #
  # config.vm.provision "chef_client" do |chef|
  #   chef.chef_server_url = "https://api.opscode.com/organizations/ORGNAME"
  #   chef.validation_key_path = "ORGNAME-validator.pem"
  # end
  #
  # If you're using the Opscode platform, your validator client is
  # ORGNAME-validator, replacing ORGNAME with your organization name.
  #
  # If you have your own Chef Server, the default validation client name is
  # chef-validator, unless you changed the configuration.
  #
  #   chef.validation_client_name = "ORGNAME-validator"
  #Vagrant.configure("2") do |config|
  #  config.vm.provision "shell",
  #    inline: "echo Hellow, World"
  #    inline: "git clone git@gitlab.com:erisinger/mhealthlab-deployable.git"
  #end

end
